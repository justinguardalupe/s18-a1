/*console.log("hello")*/
//====================> #3 to #6
// Trainer Object
let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(trainer.pokemon[0] + "!" + " I choose you!");
	}
}

console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:')
trainer.talk();

//====================> #7 to #9
// Pokemon Constructor func
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health = target.health - this.attack
		console.log(target.name+ "'s health is now reduced to " +target.health);
	if (target.health <=0) {
			console.log(target.name + " fainted");
			}
	console.log(target);
	}
}

let pikachu = new Pokemon("Pikachu", 25);
let mewtwo = new Pokemon("Mewtwo", 5);
let togepi = new Pokemon("Togepi", 100);
let ditto = new Pokemon("Ditto", 50);

console.log(pikachu);
console.log(mewtwo);
console.log(togepi);
console.log(ditto);
pikachu.tackle(mewtwo);
mewtwo.tackle(togepi);
togepi.tackle(ditto);